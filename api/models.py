from django.db import models

# Create your models here.
class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(blank=False, upload_to='static/')
    description = models.TextField(blank=False, default="Wpisz tekst...")
    location = models.CharField(blank=False, default="Podaj miasto/lokalizację", max_length=30)
    author = models.ForeignKey('auth.User', related_name = 'posts', on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        ordering = ['author']



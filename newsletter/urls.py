from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from newsletter import views

urlpatterns = [
    path('letters/', views.NewsletterList.as_view()),
    path('letters/<int:pk>/', views.NewsletterDetail.as_view()),
    path('signup/', views.NewsletterSignup, name="signup"),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)

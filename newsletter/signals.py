from typing import Optional
from django.db.models.signals import post_save
from django.dispatch import receiver
from api.models import Post
from .scripts import send_out_email


## Receiver that sends out newsletters everytime a Post model is created or updated
@receiver(post_save, sender=Post)
def send_newsletter(sender, created, updated=Optional, *args, **kwargs):
    if created or updated:
        send_out_email()
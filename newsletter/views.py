from django.shortcuts import render
from rest_framework import generics, permissions
from newsletter import serializers
from .models import NewsletterUser
from .forms import NewsletterForm

# Create your views here.

## DRF generic view to list all Newsletter subscribers - only visible to admin accounts
class NewsletterList(generics.ListAPIView):
    queryset = NewsletterUser.objects.all()
    serializer_class = serializers.NewsletterUserSerializer
    permission_classes = [permissions.IsAdminUser]

## DRF generic detail view for newsletter users, allows to edit/delete - only visible to admin accounts
class NewsletterDetail(generics.RetrieveAPIView):
    queryset = NewsletterUser.objects.all()
    serializer_class = serializers.NewsletterUserSerializer
    permission_classes = [permissions.IsAdminUser]

## Custom view for newsletter signup form
def NewsletterSignup(request):

    form = NewsletterForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        if NewsletterUser.objects.filter(email=instance.email).exists():
            return render(request, 'alrdysignedup.html')
        else:
            instance.save()
            return render(request, 'signupsuccess.html')
    
    context = {
        'form': form,
    }

    return render(request, 'newsletter.html', context)

        


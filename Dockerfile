# base image  

FROM python:3.9.2  

WORKDIR /blogdocker
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

COPY ./requirements.txt /blogdocker/

RUN pip install -r requirements.txt

COPY . /blogdocker/

CMD ["python","manage.py","runserver", "0.0.0.0:8000"]

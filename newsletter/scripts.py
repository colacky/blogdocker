from .models import NewsletterUser
from django.conf import settings
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect

## Script used to send out newsletters on trigger/signal
def send_out_email():
    email_list = []
    subscribers = NewsletterUser.objects.all()

    ## Iterate over newsletter subscribers to get up-to-date mailing list
    for subscriber in subscribers:
        email_address = subscriber.email
        email_list.append(email_address)
        print(email_list)
    ## Setup for the newsletter message
    subject = 'Pojawił się nowy wpis na naszym blogu!'
    message = 'Sprawdz jakie nowości czekają na Ciebie w najnowszym wpisie!'
    from_email = settings.EMAIL_HOST_USER
    recipient_list = email_list

    ## Newsletter send out
    send_mail(subject, message, from_email, recipient_list)

# Projekt Blog RESTApi Django - Docker

# Użyte technologie

1. Python
2. Django
3. Django REST Framework
4. PostgreSQL
5. Docker
6. HTML/CSS/BOOTSTRAP

## Setup

1. Sklonuj repozytorium z Bitbucket
2. W cmd przejdź do folderu blogdocker
3. Wpisz "docker-compose up", aby stworzyć kontener
4. Wpisz "docker ps" i sprawdź container-id bazy postgres
5. Wpisz "docker exec -i 'container-id' psql restapiblog < dumpfile -U postgres
6. Strona powinna być dostępna na localhost:8000 i podstawowe dane zaimportowane

Konto superuser dla aplikacji to login=testadmin1 i password=testing123

E-mail do aplikacji to 'apiblogtest1@gmail.com', który można zmienić w settings.py w polu EMAIL_HOST_USER.
W polę EMAIL_HOST_PASSWORD należy wprowadzić wygenerowany klucz bezpieczeństwa (możliwość wygenerowania na gmail.com) dla aplikacji niskiego bezpieczeństwa.

## Opis aplikacji

Domyślny widok aplikacji to Blog (localhost:8000)

Navbar pozwala zmienić Widoki na:

1. NewsletterSignup (/signup/) - widok pozwalający na zapisanie się do newslettera, używa domyślnej walidacji pola email w Django
2. Contact (/contact/) - widok pozwalający na wysłanie wiadomości z pytaniem do właściciela strony
3. Blog (domyślny widok) - widok przedstawiajacy posty
4. Panel admina (/admin/ - klikajac na Blog Restapi zostaniemy przeniesieni do logowania w panelu administratora

W Panelu admina dostępne są następujące widoki Django RestFramework:

- /posts/ - pokazuje posty, można sortować i filtrować po autorze, dacie dodania i dacie aktualizacji/edycji, użytkownik niezalogowany może tylko oglądać posty
- /letters/ - pokazuje aktualnych subskrybentów newslettera
- /users/ - pokazuje istniejacych uzytkowników


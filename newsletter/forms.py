from django import forms
from .models import NewsletterUser

class NewsletterForm(forms.ModelForm):
    #email = forms.EmailField()
    #name = forms.CharField(max_length=30, label="Imię")

    class Meta:
        model = NewsletterUser
        fields = ('name', 'email')

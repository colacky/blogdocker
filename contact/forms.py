from django import forms

class ContactForm(forms.Form):

    from_email = forms.EmailField(
        required=True, 
        label="Email")

    subject = forms.CharField(
        required=True, 
        label="Temat")

    message = forms.CharField(
        widget=forms.Textarea(attrs={
            "rows":5,"cols":30
        }),
        required=True, 
        label="Twoja wiadomość")
from django.shortcuts import render
from rest_framework import generics, permissions
from api import serializers
from django.contrib.auth.models import User
from api.models import Post
from api.permissions import IsOwnerOrReadOnly
from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter


# Create your views here.
class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminUser]

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminUser]

class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
    filter_backends = [filters.DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['author', 'created', 'location']
    ordering_fields = ['author', 'created', 'location']
    
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

class PostFilter(filters.FilterSet):
    location = filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = Post
        fields = ['created', 'location', 'author']
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .forms import ContactForm
from api.models import Post

# Create your views here.
def Contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['apiblogtest1@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return render(request,'success.html')
    return render(request, "contact.html", {'form': form})

def Blog(request):
    post_list = Post.objects.order_by('created')

    context = {
        'post_list' : post_list
    }

    return render(request, 'blog.html', context)